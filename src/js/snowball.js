import * as THREE from 'three'

import normalizeVector2 from './fireball/normalizeVector2'
import ForceCamera from './fireball/ForceCamera'
import ForcePointLight from './fireball/ForcePointLight'
import Mover from './fireball/Mover'
import Points from './fireball/Points'
import Util from './fireball/util'

export default function() {
  const webcam = document.getElementById('canvas-video')
  const canvas = document.getElementById('canvas-overlay')
  const renderer = new THREE.WebGL1Renderer({
    antialias: true,
    canvas: canvas,
    alpha: true
  })
  const scene = new THREE.Scene()
  const camera = new ForceCamera(35, window.innerWidth / window.innerHeight, 1, 10000)

  //
  // process for this sketch.
  //
  const movers_num = 10000
  const movers = []
  const points = new Points()
  const light = new ForcePointLight(0xff6600, 1, 1800, 1)
  const positions = new Float32Array(movers_num * 3)
  const colors = new Float32Array(movers_num * 3)
  const opacities = new Float32Array(movers_num)
  const sizes = new Float32Array(movers_num)
  const gravity = new THREE.Vector3(0, -0.3, 0)

  let last_time_activate = Date.now()

  var updateMover =  function() {
    for (var i = 0; i < movers.length; i++) {
      var mover = movers[i]
      if (mover.is_active) {
        mover.time++
        mover.applyForce(gravity)
        mover.applyDrag(0.01)
        mover.updateVelocity()
        if (mover.time > 50) {
          mover.size -= 0.7
          mover.a -= 0.009
        }
        if (mover.a <= 0) {
          mover.init(new THREE.Vector3(0, 0, 0))
          mover.time = 0
          mover.a = 0.0
          mover.inactivate()
        }
      }
      positions[i * 3 + 0] = mover.velocity.x - points.velocity.x
      positions[i * 3 + 1] = mover.velocity.y - points.velocity.y
      positions[i * 3 + 2] = mover.velocity.z - points.velocity.z
      opacities[i] = mover.a
      sizes[i] = mover.size
    }
    points.updatePoints()
  }
  var activateMover =  function() {
    var count = 0
    var now = Date.now()
    if (now - last_time_activate > 10) {
      for (var i = 0; i < movers.length; i++) {
        var mover = movers[i]
        if (mover.is_active) continue
        var rad1 = Util.getRadian(Math.log(Util.getRandomInt(0, 256)) / Math.log(256) * 260)
        var rad2 = Util.getRadian(Util.getRandomInt(0, 360))
        var range = (1- Math.log(Util.getRandomInt(32, 256)) / Math.log(256)) * 18
        var vector = new THREE.Vector3()
        var force = Util.getPolarCoord(rad1, rad2, range)
        vector.add(points.velocity)
        mover.activate()
        mover.init(vector)
        mover.applyForce(force)
        mover.a = 0.2
        mover.size = Math.pow(18 - range, 2) * Util.getRandomInt(1, 36) / 10
        count++
        if (count >= 6) break
      }
      last_time_activate = Date.now()
    }
  }
  var updatePoints =  function() {
    points.updateVelocity()
    light.obj.position.copy(points.velocity)
  }
  var movePoints = function(vector) {
    var y = vector.y * window.innerHeight / 3
    var z = vector.x * window.innerWidth / -3
    points.anchor.y = y
    points.anchor.z = z
    light.force.anchor.y = y
    light.force.anchor.z = z
  }
  var createTexture =  function() {
    var canvas = document.createElement('canvas')
    var ctx = canvas.getContext('2d')
    var grad = null
    var texture = null

    canvas.width = 200
    canvas.height = 200
    grad = ctx.createRadialGradient(100, 100, 20, 100, 100, 100)
    grad.addColorStop(0.2, 'rgba(255, 255, 255, 1)')
    grad.addColorStop(0.5, 'rgba(255, 255, 255, 0.3)')
    grad.addColorStop(1.0, 'rgba(255, 255, 255, 0)')
    ctx.fillStyle = grad
    ctx.arc(100, 100, 100, 0, Math.PI / 180, true)
    ctx.fill()

    texture = new THREE.Texture(canvas)
    texture.minFilter = THREE.NearestFilter
    texture.needsUpdate = true
    return texture
  }
  const initSketch = () => {
    for (var i = 0; i < movers_num; i++) {
      var mover = new Mover()
      var h = Util.getRandomInt(195, 210)
      var s = Util.getRandomInt(60, 90)
      var color = new THREE.Color('hsl(' + h + ', ' + s + '%, 50%)')

      mover.init(new THREE.Vector3(Util.getRandomInt(-100, 100), 0, 0))
      movers.push(mover)
      positions[i * 3 + 0] = mover.velocity.x
      positions[i * 3 + 1] = mover.velocity.y
      positions[i * 3 + 2] = mover.velocity.z
      color.toArray(colors, i * 3)
      opacities[i] = mover.a
      sizes[i] = mover.size
    }
    points.init({
      scene: scene,
      vs: `
      attribute vec3 customColor;
      attribute float vertexOpacity;
      attribute float size;

      varying vec3 vColor;
      varying float fOpacity;

      void main() {
        vColor = customColor;
        fOpacity = vertexOpacity;
        vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);
        gl_PointSize = size * (300.0 / length(mvPosition.xyz));
        gl_Position = projectionMatrix * mvPosition;
      }
      `,
      fs: `
      uniform vec3 color;
      uniform sampler2D texture;

      varying vec3 vColor;
      varying float fOpacity;

      void main() {
        gl_FragColor = vec4(color * vColor, fOpacity);
        gl_FragColor = gl_FragColor * texture2D(texture, gl_PointCoord);
      }`,
      positions: positions,
      colors: colors,
      opacities: opacities,
      sizes: sizes,
      texture: createTexture(),
      blending: THREE.AdditiveBlending
    })
    scene.add(light)
    camera.setPolarCoord(Util.getRadian(25), 0, 1000)
    light.setPolarCoord(Util.getRadian(25), 0, 200)
  }

  //
  // common process
  //
  const resizeWindow = () => {
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight)
  }
  const render = () => {
    points.applyHook(0, 0.08)
    points.applyDrag(0.2)
    points.updateVelocity()
    light.force.applyHook(0, 0.08)
    light.force.applyDrag(0.2)
    light.force.updateVelocity()
    light.updatePosition()
    activateMover()
    updateMover()
    camera.force.position.applyHook(0, 0.004)
    camera.force.position.applyDrag(0.1)
    camera.force.position.updateVelocity()
    camera.updatePosition()
    camera.lookAtCenter()
    renderer.render(scene, camera)
  }
  let animation = null
  let attempt = 0
  const renderLoop = () => {
    if (predictions[1] && predictions[1].class === 2) {
      if (attempt < 11) {
        attempt = attempt + 1
      }
      else {
        attempt = 0
        cancelAnimationFrame(animation)
        global.live = false
        return renderer.clear()
      }
    }
    else if (attempt > 0) {
      attempt = 0
    }
    render()
    animation = requestAnimationFrame(renderLoop)
  }
  const on = () => {
    const vectorTouchMove = new THREE.Vector2()

    const touchMove = (x, y, touch_event) => {
      vectorTouchMove.set(x, y)
      normalizeVector2(vectorTouchMove)
      movePoints(vectorTouchMove)
    }

    function debounce(func, wait, immediate) {
        var timeout
      
        return function executedFunction() {
          var context = this
          var args = arguments
              
          var later = function() {
            timeout = null
            if (!immediate) func.apply(context, args)
          }
      
          var callNow = immediate && !timeout
          
          clearTimeout(timeout)
      
          timeout = setTimeout(later, wait)
          
          if (callNow) func.apply(context, args)
        }
      }

    function getHandCenterPoint(bbox){
        var ratio = canvas.clientHeight/webcam.height
        if(window.innerWidth/window.innerHeight >= webcam.width/webcam.height){
            var leftAdjustment = 0
        }else{
            var leftAdjustment = ((webcam.width/webcam.height) * canvas.clientHeight - window.innerWidth)/2 
        }
        var x = bbox[0]
        var y = bbox[1]
        var w = bbox[2]
        var h = bbox[3]
        var hand_center_left = x*ratio + (w*ratio/2) - leftAdjustment
        var hand_center_top = y*ratio + (h*ratio/2)
        return [hand_center_top, hand_center_left]
    }

    const doThing = () => {
      if (predictions[1]) {
        const coords = getHandCenterPoint(predictions[1].bbox)
        touchMove(coords[1], coords[0], true)
      }      
    }

    setInterval(doThing, 100)

    window.addEventListener('resize', debounce(() => {
      resizeWindow()
    }), 1000)
  }

  const init = () => {
    renderer.setSize(window.innerWidth, window.innerHeight)
    renderer.setClearColor(0x000000, 0)
    camera.position.set(1000, 1000, 1000)
    camera.lookAt(new THREE.Vector3())

    on()
    initSketch()
    resizeWindow()
    renderLoop()
  }

  init()
}